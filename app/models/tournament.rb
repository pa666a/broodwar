class Tournament < ActiveRecord::Base
  resourcify
  belongs_to :user

  has_many :comments, dependent: :destroy

	has_many :randomized_users
	has_many :entrances
	has_many :users, -> { select("*") }, :through => :entrances

  validates :name, presence: true

	has_attached_file :replay 
	#validates_attachment_content_type :replay, :content_type => /\Areplay\/.*\rep/
	validates_attachment_content_type :replay, :not => 'text/plain', :with => %r{\.(rep)$}i

  # Map Image
  has_attached_file :map_image, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "http://placehold.it/300x300"
  validates_attachment_content_type :map_image, :content_type => /\Aimage\/.*\Z/	

  scope :ended, -> { where('ending_at < ?', DateTime.now) }
  scope :reversed, -> { order('created_at DESC') }
  
  def ended?
    !ending_at.nil? && ending_at < DateTime.now
  end
  
  def running?
    !starting_at.nil? && starting_at <= DateTime.now and DateTime.now <= ending_at
  end
  
  def upcoming?
    !starting_at.nil? && starting_at > DateTime.now
  end
end
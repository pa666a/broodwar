class User < ActiveRecord::Base
  acts_as_messageable
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
	
	validates_presence_of :nickname
  validates_uniqueness_of :nickname
	
	has_many :tournaments
  has_many :comments, through: :tournaments, dependent: :destroy
  has_many :posts
  has_many :entrances
  has_many :entrance_events, through: :entrances
  
  # Avatars
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "user-default.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  
  def name
    nickname
  end

  def mailboxer_email(object)
    nickname
  end
  
  def display_name
    "#{nickname}"
  end
  
  def profile_completed?
    !nickname.empty?
  end
  
  after_create :assign_default_role

  def assign_default_role
    add_role(:normal) if self.roles.blank?
  end
end

class ProfileController < UsersController
  def edit
  end

  def update
    if current_user.update_attributes(profile_params)
      flash[:notice]= t('ui.message.ar.updated')
      redirect_to user_path current_user
    else
      render :action => :edit
    end
  end

  private

    def profile_params
      params.require(:user).permit(:nickname, :about, :avatar)
    end
end
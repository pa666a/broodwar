class PostsController < ApplicationController
	before_filter :authenticate_user!, only: [:new, :edit, :update, :destroy]
  load_and_authorize_resource
  respond_to :html, :js
  #geocoded_by :address
  
  def index
    #TODO reversed scope
    @posts = Post.order('created_at DESC').paginate(:page => params[:page], :per_page => 4)
  end

  def show
    @post = Post.find params[:id]
  end

  def new
    @post = Post.new
  end

  def create
    @post = current_user.posts.new(post_params)
    if @post.save
      flash[:notice] = t('ui.message.ar.created')
      redirect_to :action => :show, :id => @post.id
    else
      render :action => :new
    end
  end

  def edit
    @post = Post.find params[:id]
  end

  def update
    @post = Post.find params[:id]
    if @post.update_attributes(post_params)      
      flash[:notice] = t('ui.message.ar.updated')
      redirect_to :action => :show, :id => @post.id
    else
      render :action => :edit
    end
  end

  def destroy
    post = Post.find params[:id]
    post.destroy
    flash[:notice]= t('ui.message.ar.deleted')
    redirect_to :action => :index
  end
  
  private

    def post_params
      params.require(:post).permit(:name, :description)
    end
end

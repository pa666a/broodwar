class TournamentsController < ApplicationController
	
  before_filter :authenticate_user!, only: [:new, :edit, :update, :destroy]
  load_and_authorize_resource
  respond_to :html, :js
  
  def index
    #TODO reversed scope
    @tournaments = Tournament.reversed.paginate(:page => params[:page], :per_page => 6)
  end

  def show
    @tournament = Tournament.find params[:id]
  end

  def new
    @tournament = Tournament.new
  end

  def create
    @tournament = current_user.tournaments.new(tournament_params)
    if @tournament.save
      
      flash[:notice] = t('ui.message.ar.created')
      redirect_to :action => :show, :id => @tournament.id
    else
      render :action => :new
    end
  end

  def edit
    @tournament = Tournament.find params[:id]
  end

  def update
    @tournament = Tournament.find params[:id]
    if @tournament.update_attributes(tournament_params)
      
      flash[:notice] = t('ui.message.ar.updated')
      redirect_to :action => :show, :id => @tournament.id
    else
      render :action => :edit
    end
  end

  def destroy
    tournament = Tournament.find params[:id]
    tournament.destroy
    flash[:notice]= t('ui.message.ar.deleted')
    redirect_to :action => :index
  end

  def attend
    tournament = Tournament.find params[:id]
    tournament.users << current_user unless tournament.users.include?(current_user)

    redirect_to tournament_path tournament
  end
  
  def approve_users
    @tournament = Tournament.find params[:id]
    
    respond_to do |format|
      format.js { render layout: false }
    end
  end
  
  def approve_user
    #TODO
    #entrance = TournamentsUser.find(params[:id], params[:user_id])
    #entrance.approved = false
    #entrance.save
    respond_to do |format|
      format.js { render text: 'ok' }
    end
  end
  
  def randomize_players
    tournament = Tournament.find params[:id]
    tournament.users.shuffle << randomize_player #unless tournament.users.include?(current_user)

    redirect_to tournament_path tournament
  end
  
  private

    def tournament_params
      params.require(:tournament).permit(:map_link, :map_name, :name, :description, :starting_at, :ending_at, :attend, :replay, :player1, :player2, :map_image)
    end
end
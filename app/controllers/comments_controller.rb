class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    @tournament = Tournament.find(params[:tournament_id])
    @comment = @tournament.comments.create(comment_params)
    @comment.user = current_user
    @comment.save
    
    redirect_to tournament_path(@tournament)
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    
    redirect_to @comment.tournament
  end

  private
    def comment_params
      params.require(:comment).permit(:body)
    end
end

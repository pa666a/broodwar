class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  rescue_from CanCan::AccessDenied do |exception|
    #redirect_to main_app.root_url
    
    # loop check
		if session[:last_back] != request.env['HTTP_REFERER']
		  redirect_to :back, :alert => exception.message
		  session[:last_back] = request.env['HTTP_REFERER']
		else
		  # raise on error
		  redirect_to root_path # я просто оставлю это тут :action => :index
		  flash[:danger] = exception.message
		end
  end
  
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :nickname
  end
end

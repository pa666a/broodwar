require 'test_helper'

class PostsControllerTest < ActionController::TestCase
  include Devise::TestHelpers                          

  def teardown                                         
    Warden.test_reset!                                 
  end        
  
  setup do
    @post = posts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:posts)
  end

  #test "should get new" do
  #  get 
  #  assert_response :success
  #end

  #test "should create post" do
  #  assert_difference('Post.count') do
  #    post :create, post: { description: @post.description, name: @post.name }
  #  end
#
#    assert_redirected_to post_path(assigns(:post))
#  end

  test "should show post" do
    get :show, id: @post.id
    assert_response :success
  end

  #test "should get edit" do
  #  get :edit, id: @post.id
  #  assert_response :success
  #end

  #test "should update post" do
  #  patch :update, id: @post.id, post: { description: @post.description, name: @post.name }
  #  assert_redirected_to post_path(assigns(:post))
  #end

  test "should destroy post" do
    #assert_difference('Post.count', -1) do
    #  delete :destroy, id: @post.id
    #end

    #assert_redirected_to posts_path
  end
end

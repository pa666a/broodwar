class UsersControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  def setup
    @request.env["devise.mapping"] = Devise.mappings[:admin]
    sign_in FactoryGirl.create(:admin)
  end
  
  FactoryGirl.define do
	  factory :user do
	    email { Faker::Internet.email }
	    nickname { Faker::Internet.user_name }
	    password "password"
	    password_confirmation "password"
	    confirmed_at Date.today
	  end
	end
end
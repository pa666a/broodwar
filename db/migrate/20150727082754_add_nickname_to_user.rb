class AddNicknameToUser < ActiveRecord::Migration
  def change
  	add_column :users, :nickname, :string, :unique => true
  	add_column :users, :about, :text
  end
end

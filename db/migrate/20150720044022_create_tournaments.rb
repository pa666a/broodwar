class CreateTournaments < ActiveRecord::Migration
  def change
    create_table :tournaments do |t|
      t.belongs_to :user, index: true
      t.string :name
      t.string :description
      t.string :map_name
      t.string :map_link
      t.datetime :starting_at
      t.datetime :ending_at
      t.date :date

      t.timestamps
    end
  end
end

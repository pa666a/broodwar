class CreateEntrances < ActiveRecord::Migration
  def change
    create_table :entrances do |t|
      t.belongs_to :tournament
      t.belongs_to :user, index: true
      
      t.boolean :going, null: false, default: true
      t.boolean :approved, null: false, default: true
      
      t.timestamps
    end
    
    add_index :entrances, [:tournament_id, :user_id], unique: true
  end
end

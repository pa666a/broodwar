class CreateRandomizedPlayers < ActiveRecord::Migration
  def change
    create_table :randomized_players do |t|
    	t.string :user_id
    	t.string :tournament_id
    end
  end
end

class AddVsToTournaments < ActiveRecord::Migration
  def change
    add_column :tournaments, :player1, :string
    add_column :tournaments, :player2, :string
  end
end

class AddMapImageToTournament < ActiveRecord::Migration
  def self.up
    change_table :tournaments do |t|
      t.attachment :map_image
    end
  end

  def self.down
    remove_attachment :tournaments, :map_image
  end
end

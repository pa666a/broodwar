class AddAttachmentReplayToTournaments < ActiveRecord::Migration
  def self.up
    change_table :tournaments do |t|
      t.attachment :replay
    end
  end

  def self.down
    remove_attachment :tournaments, :replay
  end
end
